package org.main;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.pages.LoginPage;

public class TaskUpdate {

	WebDriver driver;

	public void lanuchBrowser(String url) {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/main/resources/lib/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
	}

	public void login() {
		LoginPage lg = new LoginPage(driver);
		lg.login("kanchan", "khushi2912");
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions
				.elementToBeClickable(driver.findElement(By.xpath("//span[contains(text(),'kanchan')]"))));
		System.out.println(driver.findElement(By.xpath("//div/span[@id='lblcaname']")).getText());
		
	}
	
	public void taskAssign() {
		driver.findElement(By.xpath("//a[@id='5']")).click();
		driver.navigate().to("https://sangramandco.caoasoftware.com/TaskApproval.aspx");
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		
//		"CpCenter_rptData_ddlAdmin_0"

	}

	public static void main(String args[]) {

		TaskUpdate tk = new TaskUpdate();
		tk.lanuchBrowser("https://sangramandco.caoasoftware.com");
		tk.login();
		tk.taskAssign();
		//tk.driver.quit();
	}

}
