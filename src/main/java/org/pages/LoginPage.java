package org.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver=driver;
	}
	
	By uname = By.id("txtUsername");
	By pswrd = By.id("txtPassword");
	By submt = By.id("btnLogin");
	
	public void login(String usrName,String passwd ) {
		
		driver.findElement(uname).sendKeys(usrName);
		driver.findElement(pswrd).sendKeys(passwd);
		driver.findElement(submt).click();
	}

}
