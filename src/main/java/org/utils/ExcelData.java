package org.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.JsonObject;

public class ExcelData {

	public static void readData() throws Exception {
		List<String> taskList = new ArrayList<String>();
		File f = new File(System.getProperty("user.dir") + "//src//main//resources//data//Data.xlsx");
		FileInputStream file = new FileInputStream(f);
		XSSFWorkbook wb = new XSSFWorkbook(file);
		XSSFSheet sh = wb.getSheetAt(0);
		Iterator<Row> rowIterator = sh.iterator();
		Row row = rowIterator.next();
		
		while (rowIterator.hasNext()) {
			row = rowIterator.next();
			JsonObject job = new JsonObject();
			Iterator<Cell> cellIterator = row.cellIterator();
			String s = "";
			while (cellIterator.hasNext()) {
				String d = cellIterator.next().toString();
				s = s.concat(d + "|");
				System.out.println(s + "-");
			}
			taskList.add(s);
		}

		System.out.println("Total records: "+taskList.size());
		for (String s : taskList) {
			System.out.println(s);

		}
		System.out.println("Total Records: " + taskList.size());
	}

	public static void main(String args[]) throws Exception {
		final long startTime = System.nanoTime();
		readData();
		long seconds = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - startTime);
		System.out.println("Time Taken: " + seconds + "seconds");

	}
}
