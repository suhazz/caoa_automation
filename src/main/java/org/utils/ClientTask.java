package org.utils;

import java.util.Date;

public class ClientTask {

	String grpName;
	String deptName;
	String clientName;
	String taskName;
	String taskMode;
	String year;
	Date effDate;
	Date dueDate;
	String taskAdmin;
	String workEmp;
	public void ClientTask(String grName, String deptName, String clientName, String taskName,String taskMode, String year, Date effDate,
			Date dueDate, String taskAdmin, String workEmp) {
		this.grpName = grpName;
		this.deptName = deptName;
		this.clientName = clientName;
		this.taskName = taskName;
		this.taskMode = taskMode;
		this.year = year;
		this.effDate = effDate;
		this.dueDate = dueDate;
		this.taskAdmin = taskAdmin;
		this.workEmp = workEmp;

	}

}
